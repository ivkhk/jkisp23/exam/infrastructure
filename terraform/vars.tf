variable "pm_api_url" {
  type    = string
  default = ""
}

variable "pm_api_token_id" {
  type    = string
  default = ""
}

variable "pm_api_token_secret" {
  type    = string
  default = ""
}

variable "target_node" {
  type    = string
  default = ""
}

variable "clone" {
  type    = string
  default = ""
}

variable "pool" {
  type    = string
  default = ""
}
