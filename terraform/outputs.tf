output "master_ip" {
  value = proxmox_vm_qemu.master.default_ipv4_address
}

output "worker1_ip" {
  value = proxmox_vm_qemu.worker[0].default_ipv4_address
}

output "worker2_ip" {
  value = proxmox_vm_qemu.worker[1].default_ipv4_address
}
