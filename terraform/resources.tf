resource "proxmox_vm_qemu" "master" {
  name        = "K8s-Master"
  target_node = var.target_node
  memory      = 4096
  cores       = 2
  agent       = 1
  full_clone  = false
  clone       = var.clone
  boot        = "order=scsi0"
  pool        = var.pool

  network {
    bridge    = "vmbr0"
    tag       = 58
    firewall  = false
    link_down = false
    model     = "virtio"
  }

  scsihw = "virtio-scsi-single"
}

resource "proxmox_vm_qemu" "worker" {
  count       = 2
  name        = "K8s-Worker-${count.index + 1}"
  target_node = var.target_node
  memory      = 4096
  cores       = 2
  agent       = 1
  full_clone  = false
  clone       = var.clone
  boot        = "order=scsi0"
  pool        = var.pool

  network {
    bridge    = "vmbr0"
    tag       = 58
    firewall  = false
    link_down = false
    model     = "virtio"
  }

  scsihw = "virtio-scsi-single"
}
