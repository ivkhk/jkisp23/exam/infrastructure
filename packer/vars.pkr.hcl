variable "proxmox_url" {
  type = string
  default = ""
}
variable "node" {
  type = string
  default = ""
}
variable "token_id" {
  type = string
  default = ""
}
variable "token_secret" {
  type = string
  default = ""
}

variable "pool" {
  type = string
  default = ""
}

variable "vm_name" {
  type = string
  default = ""
}

variable "iso_file" {
  type = string
  default = ""
}

variable "insecure_skip_tls_verify" {
  type = bool
  default = true
}

variable "memory" {
  type = number
  default = 4096
}

variable "cores" {
  type = number
  default = 4
}

variable "scsi_controller" { 
  type = string
  default = "virtio-scsi-single"
}

variable "disk_size" {
  type = string
  default = "32G"
}

variable "storage_pool" {
  type = string
  default = ""
}

variable "discard" {
  type = bool
  default = true
}

variable "type" {
  type = string
  default = "scsi"
}

variable "bridge" {
  type = string
  default = "vmbr0"
}

variable "vlan_tag" {
  type = string
  default = "58"
}

variable "model" { 
  type = string
  default = "virtio"
}

variable "cloud_init" { 
  type = bool
  default = true
}

variable "cloud_init_storage_pool" {
  type = string
  default = "local-lvm"
} 

variable "boot_command" {
  type = list(string)
  default = [
    "<enter><wait30>",

    "sudo -i<enter>",
    "export HTTP=https://gitlab.com/ivkhk/jkisp23/exam/nixos-template-files/-/raw/main<enter>",
    "curl $HTTP/disko.nix -o /tmp/disko.nix<enter>",
    "curl $HTTP/setup.sh | bash<enter>",
  ]
}

variable "boot_wait" { 
  type = string
  default= "10s"
}

variable "unmount_iso" {
  type = bool
  default = true
}

variable "qemu_agent" {
  type = bool
  default = true
}

variable "ssh_username" {
  type = string
  default = ""
}

 variable "ssh_port" {
  type = string
  default = ""
}
 
variable "ssh_timeout" {
  type = string
  default = "10m"
}

variable "ssh_private_key" {
  type = string
  default = ""
}

variable "task_timeout" {
  type = string
  default = "10m"
}
