variables:
  ANSIBLE_ROOT: "${CI_PROJECT_DIR}/ansible"
  ANSIBLE_FORCE_COLOR: "true"
  ANSIBLE_INVENTORY: "inventory.yml"
  ANSIBLE_PLAYBOOK: "playbook.yml"
  ANSIBLE_HOST_KEY_CHECKING: "false"

  KUBERNETES_ROOT: "${CI_PROJECT_DIR}/kubernetes"


include:
  - project: 'devops_includes/gitlab_ci/packer'
    ref: main
    file: 'packer.yml'
  - project: 'devops_includes/gitlab_ci/terraform'
    file: 'terraform.yml'
    ref: main

stages:
  - build
  - format
  - lint
  - plan
  - deploy
  - output
  - provision
  - config
  - k8s-deploy
  - destroy

packer build:
  extends: .packer-job
  tags: 
    - exam

terraform fmt:
  extends: .terraform fmt
  tags: 
    - exam

terraform validate:
  extends: .terraform validate
  tags: 
    - exam

terraform plan:
  extends: .terraform plan
  tags: 
    - exam

terraform apply:
  extends: .terraform apply
  tags: 
    - exam

terraform output:
  stage: output
  extends: .terraform
  script:
    - terraform output -raw master_ip > ../master_ip.txt
    - terraform output -raw worker1_ip > ../worker1_ip.txt
    - terraform output -raw worker2_ip > ../worker2_ip.txt
  artifacts:
    paths:
      - master_ip.txt
      - worker1_ip.txt
      - worker2_ip.txt
    expire_in: 1 week
  tags:
    - exam

ansible provision:
  image: 
    name: ${CI_REGISTRY}/devops_includes/docker_images/ansible_terraform_packer:latest
    pull_policy: always
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  stage: provision
  script:
    - export MASTER_IP=$(cat master_ip.txt)
    - export WORKER1_IP=$(cat worker1_ip.txt)
    - export WORKER2_IP=$(cat worker2_ip.txt)
    - if [[ -d "$ANSIBLE_ROOT" ]]; then cd "$ANSIBLE_ROOT"; fi
    - chmod 400 ${PRIVATE_KEY}
    - sed -i "s/<MASTER_IP>/${MASTER_IP}/g" $ANSIBLE_INVENTORY
    - sed -i "s/<MASTER_USER>/${MASTER_USER}/g" $ANSIBLE_INVENTORY
    - sed -i "s/<MASTER_PORT>/${MASTER_PORT}/g" $ANSIBLE_INVENTORY
    - sed -i "s/<WORKER1_IP>/${WORKER1_IP}/g" $ANSIBLE_INVENTORY
    - sed -i "s/<WORKER2_IP>/${WORKER2_IP}/g" $ANSIBLE_INVENTORY
    - sed -i "s/<WORKER_USER>/${WORKER_USER}/g" $ANSIBLE_INVENTORY
    - sed -i "s/<WORKER_PORT>/${WORKER_PORT}/g" $ANSIBLE_INVENTORY
    - ansible-playbook -i $ANSIBLE_INVENTORY --private-key $PRIVATE_KEY $ANSIBLE_PLAYBOOK
  tags:
    - exam

kubernetes config:
  stage: config
  image: 
    name: ${CI_REGISTRY}/devops_includes/docker_images/ansible_terraform_packer:latest
    pull_policy: always
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  script:
    - export MASTER_IP=$(cat master_ip.txt)
    - chmod 400 ${PRIVATE_KEY}
    - scp -o StrictHostKeyChecking=no -i ${PRIVATE_KEY} ${MASTER_USER}@${MASTER_IP}:/etc/rancher/k3s/k3s.yaml  .
  artifacts:
    paths:
      - k3s.yaml
    expire_in: 1 week
  tags:
    - exam

kubernetes deploy:
  stage: k8s-deploy
  image: 
    name: ${CI_REGISTRY}/devops_includes/docker_images/devops_tools:latest
    pull_policy: always
    entrypoint:
      - "/usr/bin/env"
      - "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
  script:
    - export KUBECONFIG="${CI_PROJECT_DIR}/k3s.yaml"
    - until kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.14.5/config/manifests/metallb-native.yaml; do
        echo "Retrying...";
      done
    - if [[ -d "$KUBERNETES_ROOT" ]]; then cd "$KUBERNETES_ROOT"; fi
    - until kubectl apply -f loadbalancer/metallb-addresses.yaml; do
        echo "Retrying...";
      done
    - helm repo add haproxytech https://haproxytech.github.io/helm-charts
    - helm repo update
    - helm install haproxy-kubernetes-ingress haproxytech/kubernetes-ingress --create-namespace --namespace haproxy-controller --set controller.service.type=LoadBalancer
    - kubectl apply -f haproxy-ingress.yaml
    - kubectl apply -f rabbitmq.yaml
    - kubectl apply -f gateway.yaml
    - kubectl apply -f db.yaml
    - kubectl apply -f management-app.yaml
  tags:
    - exam

terraform destroy:
  extends: .terraform destroy
  tags: 
    - exam
